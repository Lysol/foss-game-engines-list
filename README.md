# List of FOSS game engine recreations

This project started as a simple list on a thread at the OpenMW forums. After a suggestion, I realized that having the list here might make updating the list a lot easier, since I don't have to manually add everything that people suggest.

I don't know yet which markup language the list will be written in.